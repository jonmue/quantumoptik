#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb  9 10:13:48 2022

@author: jonmue
"""
import numpy as np

#Aufgabe 1a)###################################################################

def rect_int(a, b, N, func):


    '''
    

    Parameters
    ----------
    a : float
        Lower boundary of integral sum.
    b : float
        Upper boundary of integral sum.
    N : integer
        Number of sampling points.
    func : function
        Lambda function to integrate over.

    Returns
    -------
    float
    integral solution

    '''

    x = np.linspace(a + (b-a)/(2*N) , b - (b-a)/(2*N), N)
    #f = getattr(np, func)(x)
    f = func(x)
    
    rect_sum = 0
    for i in range(0, N):
        rect_sum += f[i]*(b-a)/N
    
    return rect_sum

#Aufgabe 1b)###################################################################

def get_factorsTrap(a, b, N):
    '''
    

    Parameters
    ----------
    a : float
        Lower boundary of integral sum.
    b : float
        Upper boundary of integral sum.
    N : integer
        Number of sampling points.

    Returns
    -------
    x : numpy array
        "Stützstellen"
    g : numpy array
        corresponding Weighted factors to the "Stützstellen"

    '''
    x = np.linspace(a, b, N)
    
    g = np.full((N), (b-a)/(N-1))
    g[0] /= 2
    g[N-1] /= 2
    
    return x, g

def trap_int(a, b, N, func):
    '''
    

    Parameters
    ----------
    a : float
        Lower boundary of integral sum.
    b : float
        Upper boundary of integral sum.
    N : integer
        Number of sampling points.
    func : function
        Lambda function to integrate over.


    Returns
    -------
    trap_sum : float
        integral solution

    '''
    x, g = get_factorsTrap(a, b, N)
    #f = getattr(np, func)(x)
    f = func(x)
    
    trap_sum = 0
    for i in range(0, N):
        trap_sum += f[i]*g[i]
    
    return trap_sum

#Aufgabe 1c)###################################################################

def get_factorsSimp(a, b, N):
    x = np.linspace(a, b, 2*N+1)
    
    g = np.full((2*N+1), (b-a)/(3*N))
    g[1::2] *= 2
    g[0] /= 2
    g[2*N] /= 2
    
    return x, g

def simp_int(a, b, N, func):
    x, g = get_factorsSimp(a, b, N)
    f = func(x)
    
    simp_sum = 0
    for i in range(0, 2*N+1):
        simp_sum += f[i]*g[i]
    
    return simp_sum

#comparison####################################################################

#f(x) = x**2
print("f(x) = x**2, a = 0, b = 1")
print("________________________________________________________________________")
ana_square = 1/3 #analytical solution

for N in [10, 100, 1000]:
    print("Rect-Int Solution for " + str(N) +" : " + str(rect_int(0, 1, N, lambda x : x**2)))
    print("Diffence to Analytical for " + str(N) + " : " + str(rect_int(0, 1, N, lambda x : x**2) - ana_square))
    print("------------------------------------------------------------------")
    print("Trap-Int Solution for " + str(N) +" : " + str(trap_int(0, 1, N, lambda x : x**2)))
    print("Diffence to Analytical for " + str(N) + " : " + str(trap_int(0, 1, N, lambda x : x**2) - ana_square))
    print("------------------------------------------------------------------")
    print("simp-Int Solution for " + str(N) +" : " + str(simp_int(0, 1, N, lambda x : x**2)))
    print("Diffence to Analytical for " + str(N) + " : " + str(simp_int(0, 1, N, lambda x : x**2) - ana_square))
    print("##################################################################")

print("simp-Int Solution for " + str(1) +" : " + str(simp_int(0, 1, 1, lambda x : x**2)))
print("Diffence to Analytical for " + str(1) + " : " + str(simp_int(0, 1, 1, lambda x : x**2) - ana_square))
print("##################################################################")
    
#f(x) = sin(x)
print("f(x) = sin(x), a = 0, b = pi")
print("________________________________________________________________________")
ana_sin = 2 #analytical solution

for N in [10, 100, 1000]:
    print("Rect-Int Solution for " + str(N) +" : " + str(rect_int(0, np.pi, N, lambda x : np.sin(x))))
    print("Diffence to Analytical for " + str(N) + " : " + str(rect_int(0, np.pi, N, lambda x : np.sin(x)) - ana_sin))
    print("------------------------------------------------------------------")
    print("Trap-Int Solution for " + str(N) +" : " + str(trap_int(0, np.pi, N, lambda x : np.sin(x))))
    print("Diffence to Analytical for " + str(N) + " : " + str(trap_int(0, np.pi, N, lambda x : np.sin(x)) - ana_sin))
    print("------------------------------------------------------------------")
    print("simp-Int Solution for " + str(N) +" : " + str(simp_int(0, np.pi, N, lambda x : np.sin(x))))
    print("Diffence to Analytical for " + str(N) + " : " + str(simp_int(0, np.pi, N, lambda x : np.sin(x)) - ana_sin))
    print("##################################################################")