#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 19 22:55:33 2022

@author: jonmue
"""
import numpy as np
import matplotlib.pyplot as plt

#Aufgabe 2a)###################################################################

def euler(A, B, initial, N, stop):
    '''
    

    Parameters
    ----------
    A : numpy array
        Array of shape (2,2) characterizing the Matrix of the coupled odes.
    B : numpy array
        Array of shape (2,1) characterizing the Matrix of stoerfkt of the odes.
    initial : numpy array
        Initial parameters in an array of shape (3,1) in the form [[t_0],[f_1(t_0)],[f_2(t_0)]].
    N : integer
        How many values to calculate.Angab
    stop : integer
        to which t value to calculate.

    Returns
    -------
    sol_mat : numpy array
        array with (3,N) shape in the form [t, f_1(t), f_2(t)].

    '''
    sol_mat = np.zeros((3,N+1))           #initiate array of wanted size 
    sol_mat[:,0] = initial[:,0]         #set initial conditions as first array vector    
    
    dt = (stop - initial[0][0]) / N     #time step for wanted stop and N
    
    for n in range(0, N):
        sol_mat[0, n+1] = sol_mat[0, n] + dt        #calculate and add t value to sol_mat
        
        sol_mat[1:, n+1] = sol_mat[1:, n] +  dt * np.matmul(A, sol_mat[1:, n]) + dt * B[:, 0]   #calculate function values and add to sol_mat
        
    return sol_mat

#Aufgabe 2b)###################################################################    
def get_help(A, B, initial_help, dt):
    f_help = euler(A, B, initial_help, 2, initial_help[0][0] + dt/2)
    return f_help[1:, 2]
    


def improved_euler(A, B, initial, N, stop):
    sol_mat =np.zeros(((3,N+1)))
    sol_mat[:, 0] = initial[:,0]
    
    dt = (stop - initial[0][0]) / N
    
    initial_help = initial
    
    for n in range(0, N):
        
        f_help = get_help(A, B, initial_help, dt)
        sol_mat[0, n+1] = sol_mat[0, n] + dt
        sol_mat[1:,n+1] = sol_mat[1:,n] + dt * np.matmul(A, f_help) + dt * B[:, 0]
        
        initial_help = np.array(sol_mat[:, [n]])
        
    return sol_mat

#Aufgabe 2c)###################################################################



#Aufgabe 2d)###################################################################

def get_k(A, B, initial_k, dt):
    '''
    

    Parameters
    ----------
    A : numpy array
        Array of shape (2,2) characterizing the Matrix of the coupled odes.
    B : numpy array
        Array of shape (2,1) characterizing the Matrix of stoerfkt of the odes.
    initial_k : numpy array
        Initial values for [f_1(t_0), f_2(t_0)]
    dt : float
        time-step to use

    Returns
    -------
    k : numpy array
        Array for the calculated added factor in runge-kutta of 4th-order

    '''
    k1 = np.matmul(A, initial_k) + B
    k2 = np.matmul(A, initial_k + (dt/2)*k1) + B   
    k3 = np.matmul(A, initial_k + (dt/2)*k2) + B
    k4 = np.matmul(A, initial_k + dt*k3) + B
    
    k = (dt/6)*(k1 + 2*k2 + 2*k3 + k4)
    
    return k
    
    
def runge(A, B, initial, N, stop):
    '''
    

    Parameters
    ----------
    A : numpy array
        Array of shape (2,2) characterizing the Matrix of the coupled odes.
    B : numpy array
        Array of shape (2,1) characterizing the Matrix of stoerfkt of the odes.
    initial : numpy array
        Initial parameters in an array of shape (3,1) in the form [[t_0],[f_1(t_0)],[f_2(t_0)]].
    N : integer
        How many values to calculate.
    stop : integer
        to which t value to calculate.

    Returns
    -------
    sol_mat : numpy array
        array with (3,N) shape in the form [t, f_1(t), f_2(t)].

    '''
    sol_mat = np.zeros((3, N+1))    #create array for solution with format: (t:....., f1:........, f2:.......)
    sol_mat[:, 0] = initial[:,0]    #set first column to initial state
    
    dt = (stop - initial[0][0]) / N #time step
    
    initial_k = initial[1:,] #take the initial values of f_1 and f_2 for get_k

    for n in range(0, N):
        k = get_k(A, B, initial_k, dt)
        
        sol_mat[0, n+1] = sol_mat[0, n] + dt #time increase per new column
        sol_mat[1:,n+1] = sol_mat[1:,n] + k[:, 0] #use algoritm to calculate next f_1 and f_2
        
        initial_k = np.array(sol_mat[1:, [n]]) #new initial values for f_1 and f_2 set to the latest calculated
    return sol_mat



A = np.array([[0, 1],
              [-1, -0.2]])

B = np.array([[0],
              [0]])

initial = np.array([[0], 
                    [1],
                    [0]])

N = 400

stop = 7

x_euler = euler(A, B, initial, N, stop)

x_impeuler = improved_euler(A, B, initial, N, stop)

x_rk = runge(A, B, initial, N, stop)

t_ana = np.linspace(0, 7, 10000)
omega = np.sqrt(1-0.01)
x_ana = np.exp(-0.1*t_ana)*np.cos(omega*t_ana)

plt.plot(x_euler[0,:], x_euler[1,:], label = 'Euler-Verfahren')
plt.plot(x_impeuler[0,:], x_impeuler[1,:], label = 'Verbessertes-Euler-Verfahren')
plt.plot(x_rk[0,:], x_rk[1,:], label = 'Runge-Kutta-Verfahren 4ter Ordnung')
plt.plot(t_ana, x_ana, label = 'ana')
plt.legend(loc="upper left")
plt.xlabel('t in s')
plt.ylabel('x(t)')
plt.show()
#plt.savefig('harmosz.png', dpi=1000)