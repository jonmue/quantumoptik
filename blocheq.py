#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul  7 19:06:45 2022

@author: jonmue
"""
import numpy as np
import matplotlib.pyplot as plt
#from scipy import constants

#Rung-Kutta-4th ODE solver#####################################################

def get_k(A, initial_k, dt):
    '''
    

    Parameters
    ----------
    A : numpy array
        Array of shape (2,2) characterizing the Matrix of the coupled odes.
    B : numpy array
        Array of shape (2,1) characterizing the Matrix of stoerfkt of the odes.
    initial_k : numpy array
        Initial values for [f_1(t_0), f_2(t_0)]
    dt : float
        time-step to use

    Returns
    -------
    k : numpy array
        Array for the calculated added factor in runge-kutta of 4th-order

    '''
    t = initial_k[0 ,0]
    k1 = np.matmul(A(t), initial_k[1:, ])
    k2 = np.matmul(A(t + dt/2), initial_k[1:, ] + (dt/2)*k1)
    k3 = np.matmul(A(t + dt/2), initial_k[1:, ] + (dt/2)*k2)
    k4 = np.matmul(A(t + dt), initial_k[1:, ] + dt*k3)
    
    k = (dt/6)*(k1 + 2*k2 + 2*k3 + k4)
    
    return k
    
    
def runge(A, initial, N, stop):
    '''
    

    Parameters
    ----------
    A : numpy array
        Array of shape (2,2) characterizing the Matrix of the coupled odes.
    B : numpy array
        Array of shape (2,1) characterizing the Matrix of stoerfkt of the odes.
    initial : numpy array
        Initial parameters in an array of shape (3,1) in the form [[t_0],[f_1(t_0)],[f_2(t_0)]].
    N : integer
        How many values to calculate.
    stop : integer
        to which t value to calculate.

    Returns
    -------
    sol_mat : numpy array
        array with (3,N) shape in the form [t, f_1(t), f_2(t)].

    '''
    sol_mat = np.zeros((5, N+1))    #create array for solution with format: (t:....., f1:........, f2:.......)
    sol_mat[:, 0] = initial[:,0]    #set first column to initial state
    
    dt = (stop - initial[0][0]) / N #time step
    
    initial_k = initial #take the initial values of f_1 and f_2 for get_k

    for n in range(0, N):
        k = get_k(A, initial_k, dt)
        
        sol_mat[0, n+1] = sol_mat[0, n] + dt #time increase per new column
        sol_mat[1:,n+1] = sol_mat[1:,n] + k[:, 0] #use algoritm to calculate next f_1 and f_2
        
        initial_k = np.array(sol_mat[:, [n]]) #new initial values for f_1 and f_2 set to the latest calculated
    return sol_mat

###############################################################################

def coeffs(t):
    hbar = 0.66 #meV ps
    
    tau = 0.1 #ps
    E_0 = 39 #meV     d_alpa_beta* E_0
    E = E_0*np.exp(-(t / (0.6*tau))**2)
    
    T_1 = 1000 #ps
    T_2 = 1000 #ps
    
    phi = 0
    
    a1 = -1 / T_2
    a2 = phi / hbar
    a3 = 0
    a4 = 0
    
    b1 = -a2
    b2 = -a1
    b3 = -E / (2*hbar)
    b4 = -b3
    
    c1 = 0
    c2 = E / hbar
    c3 = -1 / T_1
    c4 = 0
    
    d1 = 0
    d2 = -c2
    d3 = 0
    d4 = c3
    
    A = np.array([[a1, a2, a3, a4],
                  [b1, b2, b3, b4],
                  [c1, c2, c3, c4],
                  [d1, d2, d3, d4]])
    
    return A

A = coeffs

initial = np.array([[-0.2], #ps  t_0
                    [0],    #Re(Phi_0)
                    [0],    #Im(Phi_0)
                    [0],    #n_alpha
                    [1]])   #n_beta

N = 1000

stop = 1    #ps

x = runge(A, initial, N, stop)

phi_abs = np.sqrt(x[1,:]**2 + x[2,:]**2)

#E(t) Gauß-Pulse
t = x[0,:]
E_0 = 19.5
tau = 0.1
E = E_0*np.exp(-(t / (0.6*tau))**2)


fig, (ax1, ax2, ax3, ax4) = plt.subplots(4, sharex=True)
fig.suptitle(r'$N = 1000$, $T_1 = 1000ps$, $T_2 = 1000ps$, $d_{\alpha , \beta} E_0 = 39meV$, $\tau_0 = 0.01ps$')
fig.tight_layout()
plt.xlabel('t in ps')

ax1.plot(t, E,)
ax2.plot(x[0,:], phi_abs)
ax3.plot(x[0,:], x[3,:])
ax4.plot(x[0,:], x[4,:])

ax1.set(ylabel=r'$\tilde{E}$' + ' in meV')
ax2.set(ylabel=r'$| \tilde{\Psi} |$')
ax3.set(ylabel=r'$n_\alpha$')
ax4.set(ylabel=r'$n_\beta$')
ax1.ticklabel_format(style='sci', scilimits = (0,0))
ax2.ticklabel_format(style='sci', scilimits = (0,0))
ax3.ticklabel_format(style='sci', scilimits = (0,0))
ax4.ticklabel_format(style='sci', scilimits = (0,0))


ax1.title.set_text('Gauß-Puls')
ax2.title.set_text('Betrag Übergangsamplitude')
ax3.title.set_text('Besetzung angeregter Zustand')
ax4.title.set_text('Besetzung Grundzustand')

# plt.show()
plt.savefig('blocheq14.png', dpi=1000)
